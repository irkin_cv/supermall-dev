# Bootstrap landing page test assignment.

## Building

This project uses babel.js. Make sure you have latest gulp installed.

Install dependencies:

```
npm install
bower install
```

Build release:

```
gulp
```

Your static minified deploy ready release will appear in dist folder.

## Developing

```
gulp serve
```
