$('.jumbotron .carousel').slick({
  centerMode: true,
  slidesToShow: 1,
  infinite: true,

  centerPadding: '0px',

  dots: true,
  appendDots: $('.jumbo-nav .dots'),

  arrows: true,
  prevArrow: $('.jumbo-nav .prev-button'),
  nextArrow: $('.jumbo-nav .next-button'),

  variableWidth: true,
  autoplay: true,

  // fade: true,
  cssEase: 'linear'

});
